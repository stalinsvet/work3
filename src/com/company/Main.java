package com.company;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {
        int i = 0;
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> n = new ArrayList<>();
        ArrayList<Integer> orderDiopters = new ArrayList<>();
        System.out.println("Запишите нужные диоптрии.\n " +
                "Если вы записали все нужные диоптрии напишите, стоп. ");
        while (true) {
            String workCicle = scanner.next();
            if (workCicle.toLowerCase(Locale.ROOT).equals("стоп")) {
                System.out.println("Количество циклопов которым нужны линзы:" + n.size() +
                        "\nНужные диоптрии:" + n);
                break;
            } else {
                n.add(Integer.valueOf(workCicle));
            }
        }
        outer:
        while (n.size() > 0) {

            for (int l = 1; l < n.size(); l++) {
                if (Math.abs(n.get(i) - n.get(l)) < 3) {
                    orderDiopters.add((n.get(i) + n.get(l)) / 2);
                    n.remove(l);
                    n.remove(i);
                    continue outer;
                }
            }
            orderDiopters.add(n.get(i));
            n.remove(i);
        }
        System.out.println("Пары диоптрий которые будут заказаны:" + orderDiopters);
    }
}

